import axios from "axios";
import dotenv from "dotenv";

dotenv.config();

const instance = axios.create({ baseURL: "https://api.schedule.tlwsn.dev" });
instance.defaults.headers["Authorization"] = process.env.BOT_TOKEN!;

export default instance;
