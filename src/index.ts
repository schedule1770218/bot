import { Telegraf } from "telegraf";
import { callbackQuery } from "telegraf/filters";
import dotenv from "dotenv";
import requests from "./utils/requests";
import { isValidHttpUrl } from "./utils/utils";

dotenv.config();

const botName = process.env.BOT_NAME!;

export interface ILesson {
    _id: string;
    name: string;
    teacher: string;
    audience: string;
    link: string;
    number: number;
    subgroup: number;
    day: string;
    isOnline: boolean;
    type: number;
    groups: string[];
    lessonType: string;
}

export interface IChat {
    chatId: number;
    botName: string;
    day: string;
    type: number;
    subgroup: number;
    messageId: number;
}

const days = [
    { text: "Пн", callback_data: "monday" },
    { text: "Вт", callback_data: "tuesday" },
    { text: "Ср", callback_data: "wednesday" },
    { text: "Чт", callback_data: "thursday" },
    { text: "Пт", callback_data: "friday" },
];

const typesButton = [
    { text: "Чисельник", callback_data: "ch" },
    { text: "Знаменник", callback_data: "zm" },
];

const groupsButton = [
    { text: "1 група", callback_data: "1" },
    { text: "2 група", callback_data: "2" },
    { text: "Обидві групи", callback_data: "0" },
];

const time = ["8:30 - 10:05", "10:20 - 11:55", "12:10 - 13:45", "14:30 - 16:05", "16:20 - 17:35"];

const paraEmoji = ["1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣", "🔟"];

const daysEn = ["monday", "tuesday", "wednesday", "thursday", "friday"];
const daysUa = ["Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця"];

const weeks = ["", "Чисельник", "Знаменник"];

const bot = new Telegraf(process.env.TG_TOKEN!);

async function parseRasp(day: string, type: number, subgroup: number) {
    let { data: lessons } = await requests.get<ILesson[]>(`/lessons/group/${process.env.GROUP_ID}`);

    lessons = lessons.filter((el) => el.day == day);
    lessons = lessons.filter((el) => el.type === 0 || el.type === type);
    if (subgroup !== 0)
        lessons = lessons.filter((el) => el.subgroup === subgroup || el.subgroup === 0);
    lessons.sort((a, b) => a.number - b.number);

    const index = daysEn.indexOf(day);

    let text = `${daysUa[index]}${subgroup != 0 ? ` ${subgroup} підгрупа` : ""} (${
        weeks[type]
    }):\n`;

    lessons.forEach((el) => {
        text += `\n${paraEmoji[el.number - 1]} *${el.name}*`;
        text += `\n📍 ${el.lessonType}`;
        text += `\n⏱ ${time[el.number - 1]}`;
        text += `\n👨‍🏫 ${el.teacher}`;
        // if (!el.isOnline) text += `\n🏫 ${el.audience}\n`;
        // else {
        //     if (isValidHttpUrl(el.link)) text += `\n🔗 [Посилання на пару](${el.link})\n`;
        //     else text += `\n🔗 Код доступу в Meet: ${el.link}\n`;
        // }
        text += `\n🏫 ${el.audience}`;
        if (isValidHttpUrl(el.link)) text += `\n🔗 [Посилання на пару](${el.link})\n`;
        else text += `\n🔗 Код доступу в Meet: ${el.link}\n`;

        if (subgroup === 0 && el.subgroup !== 0) text += `👥 ${el.subgroup} підгрупа\n`;
    });

    return text;
}

bot.start(async (ctx) => {
    await requests.put("/chats", {
        chatId: ctx.chat.id,
        botName,
        type: 1,
        day: "",
        subgroup: 0,
    });
    const options = {
        reply_markup: { inline_keyboard: [days] },
        disable_web_page_preview: true,
    };
    ctx.sendMessage("🗓 Оберіть день", options);
});

bot.on(callbackQuery("data"), async (ctx) => {
    const queryParams = new URLSearchParams({
        chatId: ctx.chat?.id.toString()!,
        botName,
    }).toString();
    const response = await requests.get<IChat>(`/chats?${queryParams}`);
    const info = response.data;

    const data = ctx.callbackQuery.data;
    let type = info.type;

    const checkType = ["ch", "zm"];
    const checkDay = ["monday", "tuesday", "wednesday", "thursday", "friday"];
    const checkGroup = ["1", "2", "0"];

    if (checkType.includes(data)) {
        if (data == "ch") type = 1;
        else if (data == "zm") type = 2;
        if (type == info.type && info.day != "")
            return ctx.answerCbQuery("Такий тип тижня вже обрано");
        info.type = type;
    } else if (checkDay.includes(data)) {
        if (data == info.day) return ctx.answerCbQuery("Розклад на цей день вже виведений");
        info.day = data;
    } else if (checkGroup.includes(data)) {
        if (Number(data) == info.subgroup && info.day != "")
            return ctx.answerCbQuery("Оберіть інший варіант");
        info.subgroup = Number(data);
    }

    const message = await parseRasp(info.day, type, info.subgroup);
    const keyboard = [days].concat([typesButton]).concat([groupsButton]);

    if (info.messageId != ctx.callbackQuery.message?.message_id)
        ctx.telegram.editMessageReplyMarkup(ctx.chat?.id, info.messageId, undefined, {
            inline_keyboard: [],
        });

    info.messageId = ctx.callbackQuery.message?.message_id!;
    await requests.put("/chats", info);

    ctx.editMessageText(message!, {
        parse_mode: "Markdown",
        reply_markup: { inline_keyboard: keyboard },
        disable_web_page_preview: true,
    });
});

bot.launch();

process.once("SIGINT", () => bot.stop("SIGINT"));
process.once("SIGTERM", () => bot.stop("SIGTERM"));
